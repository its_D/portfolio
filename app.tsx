import React, { FC } from 'react'

export default function App({ Page, pageProps }: { Page: FC, pageProps: Record<string, unknown> }) {
  return (
    <main>
      <head>
        <meta name="viewport" content="width=device-width" />
      </head>
      <script src="https://cdn.tailwindcss.com"></script>
      <Page {...pageProps} />
    </main>
  )
}
